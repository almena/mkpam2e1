const URL = "https://api.mlab.com/api/1/databases/bootcamp/collections/posts?apiKey=U-z_rBzfXQPvIVJ8TJnIodkwP3bdjv5t";
var response;

function obtenerPosts(){
  var peticion = new XMLHttpRequest();
  peticion.open("GET", URL, false);
  peticion.setRequestHeader("Content-Type", "application/json");
  peticion.send();

  response = JSON.parse(peticion.responseText);
  sessionStorage["posts"] = peticion.responseText;
  console.log(response);

  //mostrarPosts();
}

function mostrarPosts(){
  var tabla = document.getElementById('tablaPosts');
  for (var i = 0; i < response.length; i++){
    //alert(response[i].titulo);
    var fila = tabla.insertRow(i+1);
    var celdaId = fila.insertCell(0);
    var celdaTitulo = fila.insertCell(1);
    var celdaTexto = fila.insertCell(2);
    var celdaAutor = fila.insertCell(3);
    var celdaOperacion = fila.insertCell(4);

    celdaId.innerHTML = response[i]._id.$oid;
    celdaTitulo.innerHTML = response[i].titulo;
    celdaTexto.innerHTML = response[i].texto;
    if(response[i].autor != undefined){
      celdaAutor.innerHTML = response[i].autor;
    }
    else{
      celdaAutor.innerHTML = "anónimo";
    }
    celdaOperacion.innerHTML = '<button class=\'btn btn-primary\' type=\'button\' name=\'actualizarPost'+i+'\' onclick=\'actualizarPost("'+response[i]._id.$oid+'");\'>Actualizar</button>';
  }
}

function anadirPosts(){
  var peticion = new XMLHttpRequest();
  peticion.open("POST", URL, false);
  peticion.setRequestHeader("Content-Type", "application/json");
  peticion.send('{"titulo": "otro primer post","texto": "añadir post","autor": "Paco"}');
}

function actualizarPost(id){
  var peticion = new XMLHttpRequest();
  var URLItem = "https://api.mlab.com/api/1/databases/bootcamp/collections/posts/";
  URLItem += id;
  URLItem += "?apiKey=U-z_rBzfXQPvIVJ8TJnIodkwP3bdjv5t";

  peticion.open("PUT", URLItem, false);
  peticion.setRequestHeader("Content-Type", "application/json");
  peticion.send('{"titulo": "post actualizado","texto": "nuevo post actualiado","autor": "Paco"}');
}

function seleccionarPost(id){
  sessionStorage["id"] = id;
}

function detallePost(id){
  var posts = JSON.parse(sessionStorage["posts"]);

  for (var i = 0; i < posts.length; i++){
    if(posts[i]._id.$oid == id){
      //mostrar detalle
      document.getElementById("h1").innerHTML = id;
      document.getElementById("h2").innerHTML = posts[i].titulo;
      document.getElementById("h3").innerHTML = posts[i].text;
      break;
    }
  }
}
