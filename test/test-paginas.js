var expect = require('chai').expect;
var $ = require('chai-jquery');
var request = require('request');

describe("Pruebas sencillas", function(){
  it('Test suma', function(){
    expect(9+4).to.equal(13);
  });
});

describe("Pruebas red", function(){
  it('Test internet', function(done){
    request.get("http://www.forocoches.com",
      function(error, response, body){
        expect(response.statusCode).to.equal(200);
        done();
        //console.log("Prueba completada");
      });
  });

  it('Test localhost', function(done){
    request.get("http://localhost:8081",
      function(error, response, body){
        expect(response.statusCode).to.equal(200);
        done();
      });
  });

  it('Test body', function(done){
    request.get("http://localhost:8081",
      function(error, response, body){
        expect(body).contains('<h2>Bienvenido al blog</h2>');
        done();
      });
  });

//  it('Test contenido HTML', function(done){
//    request.get("http://localhost:8081",
//      function(error, response, body){
//        expect($("h2")).to.have.text("Bienvenido al blog");
//        done();
//      });
//  });
});
