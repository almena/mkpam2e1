'use strict';

const express = require('express');
const path = require('path');

//Constantes
const PORT = 8081;

// app
const app = express();
app.use(express.static(__dirname));
app.get('/', function(req, res){
  //res.send("Bienvenido\n");
  res.sendFile(path.join(__dirname+'/index.html'));
});

app.get('/detallePost/:id', function(req, res){
  //console.log(req);
  res.sendFile(path.join(__dirname+'/detallePost.html'));
});

app.get('/admin', function(req, res){
  //console.log(req);
  res.sendFile(path.join(__dirname+'/admin/admin.html'));
});

app.listen(PORT);
console.log("Express funcionando en el puerto" + PORT);
